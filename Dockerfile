FROM openjdk:12-alpine3.9

LABEL maintainer="Karsten Deininger <karsten.deininger@bl.ch>"

ARG version=1.11.0

ENV ILIVALIDATOR_VERSION=${version}

RUN adduser -u 1001 -G root -s /bin/bash -D validator && \
    mkdir /data && \
    chown 1001 /data && \
    chgrp 0 /data && \
    chmod g=u /data && \
    chgrp 0 /etc/passwd && \
    chmod g=u /etc/passwd && \
    apk --update add bash && \
    apk --update add --virtual .deps unzip && \
    wget https://github.com/claeis/ilivalidator/releases/download/ilivalidator-${ILIVALIDATOR_VERSION}/ilivalidator-${ILIVALIDATOR_VERSION}.zip && \
    unzip -o ilivalidator-${ILIVALIDATOR_VERSION}.zip -d /opt/ilivalidator && \
    rm -f ilivalidator-${ILIVALIDATOR_VERSION}.zip && \
    rm -rf /opt/ilivalidator/docs && \
    apk del .deps

USER 1001

COPY --chown=1001:0 uid_entrypoint.sh /usr/local/bin/

WORKDIR /data

ENTRYPOINT ["/usr/local/bin/uid_entrypoint.sh"]

CMD ["--version"]
